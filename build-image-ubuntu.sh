#!/bin/bash
podman build -f Containerfile.ubuntu -t ilhammhdd/mariadb:ubuntu-focal
podman image rm localhost:5000/ilhammhdd/mariadb:ubuntu-focal
podman tag localhost/ilhammhdd/mariadb:ubuntu-focal localhost:5000/ilhammhdd/mariadb:ubuntu-focal
podman push localhost:5000/ilhammhdd/mariadb:ubuntu-focal
