FROM ubuntu:focal

ARG MARIADB_ROOT_PASSWORD
ENV MARIADB_ROOT_PASSWORD=$MARIADB_ROOT_PASSWORD

RUN groupadd -r mysql && useradd -r -g mysql mysql

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y gpg
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ca-certificates

ARG GPG_KEYS=177F4010FE56CA3336300305F1656F24C74CD1D8

RUN set -ex; \
	export GNUPGHOME="$(mktemp -d)"; \
	for key in $GPG_KEYS; do \
		gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key"; \
	done; \
	gpg --batch --export $GPG_KEYS > /etc/apt/trusted.gpg.d/mariadb.gpg; \
	command -v gpgconf > /dev/null && gpgconf --kill all || :; \
	rm -fr "$GNUPGHOME"; \
	apt-key list

RUN set -e; \
	echo "deb https://archive.mariadb.org/mariadb-10.6.3/repo/ubuntu/ focal main" > /etc/apt/sources.list.d/mariadb.list; \
	{ \
		echo 'Package: *'; \
		echo 'Pin: release o=MariaDB'; \
		echo 'Pin-Priority: 999'; \
	} > /etc/apt/preferences.d/mariadb

RUN set -ex; \
	apt-get update; \
	apt-get install -y "mariadb-server=1:10.6.3+maria~focal" mariadb-backup; \
	rm -rf /var/lib/apt/lists/*; \
	rm -rf /var/lib/mysql; \
	mkdir -p /var/lib/mysql /var/run/mysqld; \
	chown -R mysql:mysql /var/lib/mysql /var/run/mysqld; \
	chmod 777 /var/run/mysqld;

VOLUME /var/lib/mysql

ENTRYPOINT /bin/bash

EXPOSE 3306
CMD ["mysqld"]
